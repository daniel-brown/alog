<?php
//if ($_SERVER['REMOTE_ADDR'] != "198.129.208.50") {
//        exit();
//    }

// defensively set header type before any includes.
header("Content-Type: application/rss+xml");

include("includes/initVar.php");


function str_to_rss_date($date) {
    return strftime ("%a, %d %b %Y %H:%M:%S %z", strtotime($date));
}

// If this Logbook is a closed application, only open it if the user has logged-in.
	if(oslApp::getAppInfo("appOpen") == 0 && $userID == NULL)
	{
		exit();
	}

    $appName = oslApp::getAppInfo("appName");

    $rootURL = "http".($_SERVER['HTTPS'] == 'on' ? "s" : "")."://".$_SERVER["SERVER_NAME"].str_replace("rss-feed.php", "", $_SERVER["SCRIPT_NAME"]);


    // Build SQL string.
			// Get reports.
	$get = "SELECT *, DATE_FORMAT(dateAdded,'%H:%i, %W %d %M %Y') AS 'dateAddedAdj'
    		FROM tblReports
            LEFT JOIN tblTasks ON tblReports.taskFK = tblTasks.taskID
    		LEFT JOIN tblSections ON tblTasks.sectionFK = tblSections.sectionID
    		LEFT JOIN tblUsers ON tblReports.authorFK = tblUsers.userID
    		LEFT JOIN tblValues ON tblReports.editorFK = tblValues.valueID
			WHERE postConfirmed=1 AND dateAdded > CURDATE() - INTERVAL 5 DAY
    		ORDER BY dateAdded DESC, reportID DESC
    		LIMIT 100";
    // $getRes = mysql_query($get);
    $types = "";
    $input = array();
    $getRes = oslDAO::executePrepared($get, $types, $input);

    // this causes problems if we let php parse it
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>".PHP_EOL;
?>
<rss version="2.0">
<channel>
    <title><?php echo $appName; ?> Logbook</title>
    <link><?php echo $rootURL; ?></link>
    <description>General logbook for aLIGO activities</description>
    <language>en-us</language>
<?php
    if ($getRes !== FALSE) {
        $first = TRUE;
	// Loop through and output reports.
  foreach($getRes as $loop)
	{
            if ($first) {
                echo "\t<pubDate>".str_to_rss_date($loop['dateAdded'])."</pubDate>".PHP_EOL;
                $first = FALSE;
            }
	    // Initialise.
	    $reportID = $loop["reportID"];
	    $reportTitle = $loop["reportTitle"];
	    $parentFK = $loop["parentFK"];
            $link = $rootURL."index.php?callRep=$reportID";

            $author = str_replace("@LIGO.ORG", "@ligo.org", $loop['username']);
	    $reportHeader = "<p>Author: $author</p><p>Report ID: $reportID</p>";
?>

        <item>
            <title><?php echo htmlspecialchars(iconv("ISO-8859-1//TRANSLIT","UTF-8",$loop['sectionName']." ".$loop['taskName']." - ".$reportTitle)); ?></title>
            <link><?php echo $link; ?></link>
            <description><?php
            echo htmlspecialchars(iconv("ISO-8859-1//TRANSLIT","UTF-8",$reportHeader.$loop['reportText']));
            echo htmlspecialchars(iconv("ISO-8859-1//TRANSLIT","UTF-8", oslContent::outputReportTags($reportID, NULL)));
            // Display attached images.
            echo htmlspecialchars(iconv("ISO-8859-1//TRANSLIT","UTF-8",oslFile::displayFiles($reportID,1,0,1,FALSE,2)));
            // Display other files.
            echo htmlspecialchars(iconv("ISO-8859-1//TRANSLIT","UTF-8",oslFile::displayFiles($reportID,1,0,0,FALSE,2)));

            ?></description>
            <guid><?php echo $link; ?></guid>
            <author><?php echo $author; ?></author>
            <pubDate><?php echo str_to_rss_date($loop['dateAdded']); ?></pubDate>
            <category><?php echo htmlspecialchars(iconv("ISO-8859-1//TRANSLIT","UTF-8",$loop['sectionName']." ".$loop['taskName'])); ?></category>
        </item>
<?php
        }
    }
?>
</channel>
</rss>
