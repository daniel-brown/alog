/*
Copyright (C) 2010,  European Gravitational Observatory.

This file is part of OSLogbook.

OSLogbook is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

This file was written by Gary Hemming <gary.hemming@ego-gw.it>.
*/

// Open AJAX sack.
var ajax = new sack();

/* Update tasks. */
function updateTasks(search)
{
	// Get current section ID.
	var reportSection = document.getElementById("reportSection").value;
	// Reset tasks to null.
	document.getElementById("reportTask").options.length = 0;
	if(search == 1)
	{
		obj = document.getElementById("reportTask");
		obj.options[obj.options.length] = new Option('','');
	}
	// Get tasks related to this part in the database.
	ajax.requestFile = 'scripts/updateTasks.php?qRepID='+reportSection;	// Specify which file to get
	ajax.onCompletion = completeUpdateTasks;	// Specify function that will be executed after file has been found
	ajax.runAJAX();		// Execute AJAX function
}

function completeUpdateTasks()
{
	var obj = document.getElementById("reportTask");
	eval(ajax.response);	// Executing the response from Ajax as Javascript code	
}

// Edit individual task name.
function editTask()
{
	var Index = document.getElementById("reportTask").selectedIndex;
	document.getElementById("adTaskName").value = document.getElementById("reportTask").options[Index].text;
	// Get task value.
	var val = document.getElementById("reportTask").selectedIndex;
	// If a value now exists.
	if(val != 0 && val != '')
	{
		// Make delete task button visible.
		document.getElementById("delTaskBtn").className = "buttonVisible";
	}
	// Otherwise.
	else
	{
		// Hide delete task button.
		document.getElementById("delTaskBtn").className = "hidden";
	}

}

// Redirect to delete individual task.
//function delTask()
//{
//	var taskID = document.getElementById("reportTask").value;
//	if(confirm("Are you sure you want to delete this task?"))
//	{
//		window.top.location="includes/confirmation.php?adminType=delTask&reportTask=" + taskID;
//	}
//}

// Update task name in database.
//function updateTaskName()
//{
//	var taskID = document.getElementById("reportTask").value;
//	var sectionID = document.getElementById("edSectionID").value;
//	var url = "includes/confirmation.php?adminType=addTask&reportTask=" + taskID + "&edSectionID=" + sectionID;
//	document.forms["frmaddTask"].action = url;
//	document.forms["frmaddTask"].submit();
//	return true;
//}

// new function to update/add tasks
function validateTaskForm(frmName)
{
	var taskID = document.getElementById("reportTask").value;
	var sectionID = document.getElementById("edSectionID").value;
	var taskName = document.getElementById("adTaskName").value;
	
	var frm = document.forms[frmName]
	frm.elements["reportTask"].value = taskID;
	frm.elements["edSectionID"].value = sectionID;
	frm.elements["adTaskName"].value = taskName;
	
	if (sectionID == '' || taskName == '') {
		return false;
	}	
	return true;
}

function submitTaskForm(frmName)
{
	if (validateTaskForm(frmName)) {
		document.forms[frmName].submit()
		return true
	}
	return false
}

function validateUpdateTaskForm()
{
	return validateTaskForm('frmupdateTaskSingle')
}

function submitUpdateTaskForm()
{
	return submitTaskForm('frmupdateTaskSingle')
}

function validateDeleteTaskForm()
{
	return validateTaskForm('frmdeleteTaskSingle')
}

function submitDeleteTaskForm()
{
	if (confirm("Are you sure you want to delete this task?")) {
		return submitTaskForm('frmdeleteTaskSingle')
	}
	return false
}