/*
Copyright (C) 2010,  European Gravitational Observatory.

This file is part of OSLogbook.

OSLogbook is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

This file was written by Gary Hemming <gary.hemming@ego-gw.it>.
*/

// Open AJAX sacks.
var ajaxB = new sack();

/* Update sections. */
function editSection()
{
	// Get current section ID.
	var sectionID = document.getElementById("edSectionID").value;
	// Reset tasks to null.
	obj = document.getElementById("reportTask");
	obj.options.length = 0;
	obj.options[obj.options.length] = new Option('','');
	// Reset add section to null.
	document.getElementById("adSectionName").value = '';
	// Display hide delete option.
	if(sectionID == '')
	{
		document.getElementById("delSectionButton").className="hidden";
		document.getElementById("sectionTxt").innerHTML = "Add section name";
		document.getElementById("taskHdr").innerHTML = "";
		document.getElementById("taskTxt").innerHTML = "";
		document.getElementById("adTaskName").className = "hidden";
		document.getElementById("saveTaskButton").className = "hidden";
		document.getElementById("taskHdrB").innerHTML = "";
		document.getElementById("reportTask").className = "hidden";
		document.getElementById("delTaskBtn").className = "hidden";
		document.getElementById("adTaskName").value = "";
	}
	else
	{
		document.getElementById("delSectionButton").className="buttonVisible";
		document.getElementById("sectionTxt").innerHTML = "Edit section name";
		document.getElementById("taskHdr").innerHTML = "<strong>Tasks</strong>";
		document.getElementById("taskTxt").innerHTML = "Add new task";
		document.getElementById("adTaskName").className = "visible";
		document.getElementById("saveTaskButton").className = "buttonVisible";
		document.getElementById("taskHdrB").innerHTML = "Select task to edit";
		document.getElementById("reportTask").className = "visible";
		document.getElementById("adTaskName").value = "";
	}
	// Get called section name.
	ajaxB.requestFile = 'scripts/editSection.php?qSectID='+sectionID;	// Specify which file to get
	ajaxB.onCompletion = updateSection;	// Specify function that will be executed after file has been found
	ajaxB.runAJAX();		// Execute AJAX function
	// Update task list.
	ajax.requestFile = 'scripts/updateTasks.php?qRepID='+sectionID;	// Specify which file to get
	ajax.onCompletion = completeUpdateTasks;	// Specify function that will be executed after file has been found
	ajax.runAJAX();		// Execute AJAX function
}

function updateSection()
{
	var obj = document.getElementById("adSectionName");
	eval(ajaxB.response);	// Executing the response from Ajax as Javascript code
}

// Redirect to delete individual section.
function delSection()
{
	var sectionID = document.getElementById("edSectionID").value;
	if(confirm("Are you sure you want to delete this section?"))
	{
		window.top.location="includes/confirmation.php?adminType=delSection&edSectionID=" + sectionID;
	}
}

// Update section name in database.
function updateSectionName()
{
	var sectionID = document.getElementById("edSectionID").value;
	var url = "includes/confirmation.php?adminType=addSection&edSectionID=" + sectionID;
	document.forms["frmaddSection"].action = url;
	document.forms["frmaddSection"].submit();
	return true;
}

// direct enter keypresses to the correct location
function sectionProcessKey(event)
{
    if (null == event)
        event = window.event ;
    if (event.keyCode == 13)  {
        document.getElementById("saveButton").click();
        return false;
    }
}
