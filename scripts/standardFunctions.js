/*
Copyright (C) 2010,  European Gravitational Observatory.

This file is part of OSLogbook.

OSLogbook is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

This file was written by Gary Hemming <gary.hemming@ego-gw.it>.
*/

///////////////////////////
// Javascript functions //
/////////////////////////

/* Send to requested location. */
function redirect(form,way)
{
	// Set location to form and submit.
	if(form != '')
	{
		document.forms[form].action=way;
		document.forms[form].submit();
	}
	else
	{
		window.top.location = way;
	}
}

// Request confirmation before going to URL.
function requestConfirmation(str,way)
{
	// If confirmation is provided, send the user on.
	if(confirm(str))
	{
		redirect('',way);
	}
}

// Clear input box text.
function clearInputBox(fieldName)
{
	if (fieldName.defaultValue == fieldName.value)
	{
	  	fieldName.value = "";
	}
}

// Re-size iframe to fit screen.
function autoResizeIframe()
{
	// Init.
	var y_main = document.getElementById('main').offsetHeight;
	var y_upper = document.getElementById('upper').offsetHeight;
	var y_links = document.getElementById('links').offsetHeight;
	var y_borderBottom = document.getElementById('borderBottom').offsetHeight;
	var borders = 4;
	// Calculate delta.
	var delta = y_main - (y_upper + y_links + y_borderBottom + borders);
	// Re-size iframe.
	document.getElementById('ifrMain').height = delta;
}

// direct enter keypresses to the correct location
function processEnterKey(event, objId)
{
    if (null == event)
        event = window.event ;
    if (event.keyCode == 13)  {
        document.getElementById(objId).click();
        return false;
    }
	return true;
}