<?php
/*
Copyright (C) 2010,  European Gravitational Observatory.

This file is part of OSLogbook.

OSLogbook is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

This file was written by Gary Hemming <gary.hemming@ego-gw.it>.
*/

//////////////////////
// User management //
////////////////////

class oslUser
{
	// Get userID.
	static function getUserDetailFromUsername($username, $field)
	{
		// Init.
		$val = NULL;
		// If both username and password exist.
		if ($username != NULL) {
			// Get user-related field.
			// $sql = oslDAO::executeQuery("SELECT $field
			// 							 FROM tblUsers
			// 							 WHERE username LIKE '$username'
			// 							 LIMIT 1");
			$sql = "SELECT $field FROM tblUsers WHERE username LIKE ? LIMIT 1";
			$types = "s";
			$input = array($username);
			$rows = oslDAO::executePrepared($sql, $types, $input);
			// Loop.
			// while($loop = mysqli_fetch_array($sql))
			foreach ($rows as $loop) {
				$val = $loop["$field"];
			}
		}
		// Return.
		return $val;
	}
	// Get user info.
	static function getUserInfo($field, $userID)
	{
		// Init.
		$val = NULL;
		// If user exists.
		if ($userID != NULL && $userID != 0) {
			// Get user-related field.
			// $sql = oslDAO::executeQuery("SELECT $field
			// 							 FROM tblUsers
			// 							 LEFT JOIN tblUserGroups ON tblUsers.userGroupFK = tblUserGroups.userGroupID
			// 							 WHERE userID=$userID
			// 							 LIMIT 1");

			$sql = "SELECT $field FROM tblUsers LEFT JOIN tblUserGroups
              ON tblUsers.userGroupFK = tblUserGroups.userGroupID
              WHERE userID=? LIMIT 1";
			$types = "i";
			$input = array($userID);
			$rows = oslDAO::executePrepared($sql, $types, $input);
			// Loop.
			// while($loop = mysqli_fetch_array($sql))
			foreach ($rows as $loop) {
				$val = $loop["$field"];
			}
		}
		// Return.
		return $val;
	}

	// Authenticate user.
	static function authenticateUser($username, $password)
	{
		// Init.
		$authenticated = FALSE;
		// Get log-in method.
		$loginMethod = oslApp::getAppInfo("appLoginMethod");
		// If user is not forbidden and either, both username and password exist or log-in method is Apache and the username exists.
		if (
			oslUser::getUserDetailFromUsername($username, "userGroupFK") != 5
			&& (($username != NULL && $password != NULL)
				|| ($loginMethod == 3 && $username != NULL))
		) {
			// Check if access is restricted to specific users.
			$appRestrict = oslApp::getAppInfo("appRestrict");
			if (
				$appRestrict == 0
				|| ($appRestrict == 1 && oslUser::checkIfUserExists($username))
			) {
				// If UNIX.
				if ($loginMethod == 1) {
					// Init domain
					$domain = yp_get_default_domain();
					// Check against UNIX values.
					$entry = @yp_match($domain, "passwd.byname", $username);
					// If UNIX values found.
					if ($entry) {
						list($user, $pass, $uid, $gid, $gecos, $home, $shell) = explode(":", $entry);
						// If user has logged-in successfully and is not forbidden.
						if ($pass == crypt($password, substr($pass, 0, 2))) {
							// Set authenticated to TRUE.
							$authenticated = TRUE;
						}
					}
				}
				// If LDAP.
				elseif ($loginMethod == 2) {
					// Init host and DN.
					$host = oslApp::getAppInfo("ldapHost");
					$dn = oslApp::getAppInfo("ldapDN");
					// Attempt LDAP connection.
					$ad = ldap_connect("ldap://$host");
					// Set protocol version.
					ldap_set_option($ad, LDAP_OPT_PROTOCOL_VERSION, 3);
					// Set referrals version.
					ldap_set_option($ad, LDAP_OPT_REFERRALS, 0);
					// Bind to host.
					if (ldap_bind($ad, "$username@$host", "$password")) {
						// If bind successful, set authenticated to true.
						$authenticated = TRUE;
					}
				}
				// If Logbook DB.
				elseif ($loginMethod == 0) {
					$query = "SELECT * FROM `tblUsers` WHERE username=? AND password=? AND username <> '' AND password <> '' LIMIT 1";
					$types = "ss";
					$args = array($username, $password);
					$rows = oslDAO::executePrepared($query, $types, $args);
					if (count($rows) > 0) {
						$authenticated = TRUE;
					}
				}
				// If Apache.
				elseif ($loginMethod == 3) {
					// Get remote user variable.
					$rUser = getenv('REMOTE_USER');
					// Authenticated username exists.
					if (!empty($rUser)) {
						$authenticated = TRUE;
					}
				}
				// If authenticated.
				if ($authenticated == TRUE) {
					oslUser::logTime($username);
				}
			}
		}
		// Return.
		// if($authenticated == FALSE)
		// {
		//   echo "The username and/or password are incorrect.";
		// }
		return $authenticated;
	}

	// Set the username.
	static function setUsername($post_input)
	{
		// Init.
		$username = NULL;
		// If logging-in via Apache, set username as remote_user.
		if (oslApp::getAppInfo("appLoginMethod") == 3) {
			// Get remote user.
			$rUser = getenv('REMOTE_USER');
			// If remote user exists, set it to username.
			if (!empty($rUser)) {
				$username = $rUser;
			}
		}
		// Otherwise.
		else {
			$username = $post_input["username"];
		}
		// Return.
		return $username;
	}

	// Set the username and autologin if possible (ie in apache auth)
	static function setUserNameAndLogin($post_input, $callDB)
	{
		global $authExpired;

		// let setUsername do the heaving lifting
		$username = oslUser::setUsername($post_input);
		echo $username;
		// get the login method
		$appLoginMethod = oslApp::getAppInfo("appLoginMethod");
		// attempt auto-login if it hasn't already failed and apache auth is used and we have a username
		if ($username && $appLoginMethod == 3 && !isset($_SESSION["user" . $callDB . "ID"]) && !isset($_SESSION["loginFailed"])) {
			// Authenticate user.
			if (oslUser::authenticateUser($username, $password)) {
				// Set the userID session.
				$_SESSION["user" . $callDB . "ID"] = oslUser::getUserDetailFromUsername($username, "userID");
			} else {
				$_SESSION["loginFailed"] = 1;
			}
		} elseif (!$username && $appLoginMethod == 3 && isset($_SESSION["user" . $callDB . "ID"]) && !isset($_SESSION["loginFailed"])) {
			// Apache auth has ended but the user had logged in
			// for now set the username and make it look like all is logged in
			// this is a quick fix to an issue we are seeing when a
			// user keeps their browser open for extended periods of time
			// and the apache auth (via shibboleth) expires
			//$username = oslUser::getUserInfo("username", $_SESSION["user".$callDB."ID"]);

			// It is very important that we do NOT kill any existing login data at this point.
			// By allowing the login to continue even when the auth is expired it allows us to
			// save any pending data and prevent data loss.  This does mean that
			// the login details will have to be killed in another location.
			$authExpired = 1;
		} else {
			if (!$username && $appLoginMethod != 3 && isset($_SESSION["user" . $callDB . "ID"]) && !isset($_SESSION["loginFailed"])) {
				$username = oslUser::getUserInfo(username, $_SESSION["user" . $callDB . "ID"]);
				//print("Setting username from session info");
				//die();
			}
		}
		return $username;
	}

	// Check if a username exists.
	static function checkIfUserExists($username)
	{
		// Init.
		$exists = FALSE;
		// Get.
		$query = "SELECT userID FROM `tblUsers` WHERE username=? AND username <> '' LIMIT 1";
		$types = "s";
		$args = array($username);
		$rows = oslDAO::executePrepared($query, $types, $args);
		// Check.
		foreach ($rows as $loop) {
			$exists = TRUE;
		}
		// Return.
		return $exists;
	}

	// Log the time of last log-in.
	static function logTime($username)
	{
		// If username exists.
		if ($username != NULL) {
			// If the user already exists.
			if (oslUser::checkIfUserExists($username)) {
				// Update.
				oslUser::updateLoginTime($username);
			} else {
				// Insert.
				oslUser::insertUser($username);
			}
		}
	}

	// Update log-in time.
	static function updateLoginTime($username)
	{
		// Init.
		$now = date("Y-m-d H:i:s");
		// Update.
		$query = "UPDATE tblUsers SET lastLogin=? WHERE username LIKE ? AND username NOT LIKE ''";
		$types = "ss";
		$args = array($now, $username);
		oslDAO::executePrepared($query, $types, $args);
	}

	// Insert user.
	static function insertUser($username)
	{
		// Init.
		$now = date("Y-m-d H:i:s");
		// $username = oslDAO::filterForMySQL($username);
		// Update.
		$query = "INSERT INTO tblUsers (username, lastLogin, password, forename, surname, email) VALUES (?, ?, ?, ?, ?, ?)";
		$types = "ssssss";
		// FIXME: perhaps use a something random for password in the future
		//        for now, our Shibboleth configuration handles this, so the
		//        password field goes unreferenced in practice at LIGO
		$args = array($username, $now, '', '', '', $username);
		oslDAO::executePrepared($query, $types, $args);
	}

	// Add user.
	static function addUser($userID, $input_post)
	{
		// Init.
		$url = "../index.php?content=8";
		// If admin and user has been sent.
		if (oslUser::checkIfAdmin($userID)) {
			// Filter the posted array.
			$input_post = oslDAO::filterArray($input_post);
			// $fields = array('username','password','forename','surname','email','userGroupFK');
			$username_val = $input_post[username];
			$password_val = $input_post[password];
			$forename_val = $input_post[forename];
			$surname_val = $input_post[surname];
			$email_val = $input_post[email];
			$userGroupFK_val = $input_post[userGroupFK];
			$sql = "INSERT INTO tblUsers (username,password,forename,surname,email,userGroupFK) VALUES (?, ?, ?, ?, ?, ?)";
			$types = "sssssi";
			$input = array($username_val, $password_val, $forename_val, $surname_val, $email_val, $userGroupFK_val);
			$rows = oslDAO::executePrepared($sql, $types, $input);
		}
		// 	// Insert new Code into tblCodes.
		// 	// $sql = oslDAO::executeQuery("INSERT INTO tblUsers
		// 	// 					 		 ($fields)
		// 	// 					 		 VALUES
		// 	// 					 		 ($values)");
		// }

		// Return.
		return $url;
	}

	// Update editor instance to be used.
	static function updateEdInst($callUser)
	{
		// Get variables set elsewhere.
		global $editorInstance;
		// If user and instance sent.
		if ($callUser != NULL && $callUser != 0 && $editorInstance != NULL) {
			$query = "UPDATE tblUsers SET defaultEditor=? WHERE userID=?";
			$types = "si";
			$args = array($editorInstance, $callUser);
			oslDAO::executePrepared($query, $types, $args);
		}
	}

	// Update user.
	static function updateUser($userID, $input_post, $callUser)
	{
		// Init.
		$url = "../index.php?content=8";
		// If admin and user have been sent.
		if (oslUser::checkIfAdmin($userID) && $callUser != NULL && $callUser != 0) {
			// Filter the posted array.
			$input_post = oslDAO::filterArray($input_post);
			// Update for each posted value.
			foreach ($input_post as $key => $val) {
				// $val = oslDAO::filterForMySQL($val);
				// Run SQL.
				// $sql = oslDAO::executeQuery("UPDATE tblUsers
				// 					 		 SET $key='$val'
				// 					 		 WHERE userID=$callUser");
				$sql = "UPDATE tblUsers SET $key=? WHERE userID=?";
				$types = "si";
				$input = array($val, $callUser);
				$rows = oslDAO::executePrepared($sql, $types, $input);
			}
		}
		// Return.
		return $url;
	}

	// Update user email.
	static function updateUserEmail($userID, $input_post)
	{
		// Init.
		$url = "../index.php?content=5";
		// If user has been sent.
		if ($userID != NULL && $userID != 0) {
			// Filter the posted array.
			$input_post = oslDAO::filterArray($input_post);
			// Update for each posted value.
			$sql = "UPDATE tblUsers SET email=? WHERE userID=?";
			$types = "si";
			$input = array($input_post['email'], $userID);
			$rows = oslDAO::executePrepared($sql, $types, $input);
		}
		// Return.
		return $url;
	}

	// Delete user.
	static function deleteUser($userID, $callUser)
	{
		// Init.
		$url = "../index.php?content=8";
		// If admin and user has been sent.
		if (oslUser::checkIfAdmin($userID) && $callUser != NULL && $callUser != 0) {
			// Run SQL.
			$query = "DELETE FROM tblUsers WHERE userID=?";
			$types = "i";
			$args = array($callUser);
			oslDAO::executePrepared($query, $types, $args);
		}
		// Return.
		return $url;
	}

	// Build array of tasks for which the user has requested mail notification.
	static function getUserMN($userID)
	{
		// Init.
		$array = array();
		// Get mail notification tasks for this user.
		$query = "SELECT taskFK FROM tblMailNotification WHERE userFK=?";
		$types = "i";
		$args = array($userID);
		$rows = oslDAO::executePrepared($query, $types, $args);
		// Build userMN array.
		foreach ($rows as $loop) {
			$taskFK = $loop["taskFK"];
			$array[$taskFK] = $taskFK;
		}
		// Return.
		return $array;
	}

	// Check if user is admin.
	static function checkIfAdmin($callUser)
	{
		// Init.
		$admin = FALSE;
		// If user exists.
		if ($callUser != NULL && $callUser != 0) {
			// If user group is 1.
			if (oslUser::getUserInfo("userGroupFK", $callUser) == 1) {
				$admin = TRUE;
			}
		}
		// Return.
		return $admin;
	}

	// Check if a user has access to content.
	static function checkContentAccess($callUser, $callContent)
	{
		global $authExpired;

		// Init.
		$granted = FALSE;
		if ($callContent != NULL && $callContent != 0) {
			// Check that it actually exists in the database.
			if (
				$callContent >= oslContent::getContentRange($callContent, "ASC")
				&& $callContent <= oslContent::getContentRange($callContent, "DESC")
			) {
				// Get the content access level.
				$level = oslContent::getContentDetails($callContent, "accessLevel");
				// If content is open.
				if ($level == 0) {
					// Permission granted.
					$granted = TRUE;
				}
				// Otherwise.
				{
					// expired credentials are not allowed to access restricted areas
					if (!$authExpired) {
						// If user is admin
						if (oslUser::checkIfAdmin($callUser)) {
							// Permission granted.
							$granted = TRUE;
						}
						// Otherwise.
						else {
							// If log-in required and user is logged-in.
							if ($level == 2 && $callUser != NULL) {
								$granted = TRUE;
							}
						}
					}
				}
			}
		}
		// Return.
		return $granted;
	}

	// Output L-mail.
	static function outputLmail($callUser, $callContent, $tabs)
	{
		global $sendLMail;

		// If Administrator.
		if ($callUser != NULL && $callUser != 0) {
			if ($sendLMail) {
				// Add number of tabs required.
				$tabStr = oslStructure::getRequiredTabs($tabs);
				// Output title.
				$title = oslContent::getContentTitle($callContent, "contentName");
				$adUserEmail = oslUser::getUserInfo("email", $callUser);
				$userMNArray = oslUser::getUserMN($callUser);
				// Output header.
				$str .= "$tabStr		<p><img src=\"images/greenSquareShadowed.gif\" alt=\"\" class=\"greenSquareShadowed\" />L-Mail is the email alert system used by the Logbook application. The information provided below enables the Logbook to automatically send email alerts when reports are posted in specific tasks.</p>\n";
				$str .= "$tabStr		<p><img src=\"images/greenSquareShadowed.gif\" alt=\"\" class=\"greenSquareShadowed\" />In order to take advantage of this option, enter your email address and select the tasks of interest.</p>\n";
				// Open form and output email update option.
				$str .= "$tabStr		<!-- Open frmAppName form. -->\n";
				$str .= "$tabStr		<form name=\"frmAppName\" id=\"frmAppName\" method=\"post\">\n";
				// Open coverDiv
				$str .= oslStructure::openDiv("divCoverTop", $tabs + 2, "coverDiv");

				$str .= "$tabStr			<div class=\"coverHdrDiv\">Email address</div>\n";
				$str .= "$tabStr			<div class=\"coverDetailsDiv\">\n";
				$str .= "$tabStr				<input type=\"text\" name=\"email\" id=\"email\" value=\"$adUserEmail\" class=\"inputEmail\" />\n";
				$str .= oslStructure::getButton("btnSaveEmailAddress", "SAVE EMAIL ADDRESS", "frmAppName", "includes/confirmation.php?adminType=editUserEmail", NULL, NULL, $tabs + 4);
				$str .= "$tabStr			</div>\n";
				// Close coverDiv
				$str .= oslStructure::closeDiv("divCoverTop", $tabs + 2);
				$str .= "$tabStr		<!-- Close frmAppName form. -->\n";
				$str .= "$tabStr		</form>\n";
				// Open coverDiv
				$str .= oslStructure::openDiv("divCoverBottom", $tabs + 2, "coverDiv");
				// Output sections and tasks.
				$str .= "$tabStr			<!-- Open frmMailNotification form. -->\n";
				$str .= "$tabStr			<form name=\"frmMailNotification\" id=\"frmMailNotification\" method=\"post\">\n";
				$str .= "$tabStr			<table cellpadding=\"0\" cellspacing=\"1\" border=\"0\" class=\"adminTable\">\n";
				$str .= "$tabStr				<tr>\n";
				$str .= "$tabStr					<td class=\"tdVal\"><strong>Choose tasks from the list below:</strong></td>\n";
				$str .= "$tabStr				</tr>";
				// Get all sections.
				$query = "SELECT * FROM tblSections ORDER BY sectionName";
				$types = "";
				$args = array();
				$rows = oslDAO::executePrepared($query, $types, $args);
				// Loop.
				foreach ($rows as $loop) {
					// Init.
					$sectionID = $loop["sectionID"];
					$sectionName = $loop["sectionName"];
					$sectionColour = $loop["sectionColour"];
					$str .= "$tabStr				<tr>\n";
					$str .= "$tabStr					<td class=\"tdCheckbox\" bgcolor=\"#$sectionColour\"> $sectionName			</td>\n";
					$str .= "$tabStr				</tr>";
					// If this section has tasks output them here.
					$query = "SELECT * FROM tblTasks WHERE sectionFK = ? ORDER BY taskName";
					$types = "i";
					$args = array($sectionID);
					$rowsT = oslDAO::executePrepared($query, $types, $args);
					// Loop.
					foreach ($rowsT as $tLoop) {
						// Init.
						$taskID = $tLoop["taskID"];
						$taskName = $tLoop["taskName"];
						// Check if this should be checked.
						$checkDef = NULL;
						if (in_array($taskID, $userMNArray)) {
							$checkDef = "checked";
						}
						$str .= "$tabStr				<tr>\n";
						$str .= "$tabStr					<td class=\"tdCheckbox\">\n";
						$str .= "$tabStr						<input type=\"checkbox\" id=\"MNTaskFK$taskID\" name=\"MNTaskFK$taskID\" class=\"loginButton\" value=\"$taskID\" $checkDef /> $taskName</td>\n";
						$str .= "$tabStr				</tr>\n";
					}
				}
				// Close form and table.
				$str .= "$tabStr			<!-- Close frmMailNotification form. -->\n";
				$str .= "$tabStr			</form>\n";
				$str .= "$tabStr			</table>\n";
				// Get button.
				$str .= oslStructure::getButton("btnSaveTasks", "SAVE TASKS", "frmMailNotification", "includes/confirmation.php?adminType=editMailNotification", NULL, NULL, $tabs + 4);
				// Close coverDiv
				$str .= oslStructure::closeDiv("divCoverBottom", $tabs + 2);
				// Add break.
				$str .= oslStructure::getBreak($tabs + 2);
			} else {
				$title = "L-mail";
				$str = "L-Mail is the email alert system.  It has been administratively disabled.";
			}
			// Output.
			echo oslStructure::getRoundedContentDiv("divSearch", $title, $str, $tabs);
		}
	}

	// Mail notification.
	static function mailNotification($userID, $input_post)
	{
		// Init.
		$url = "../index.php?content=5";
		// If the user is Admin.
		if ($userID != NULL && $userID != 0) {
			// First of all, delete all mail notifications related to this user.
			// $del = oslDAO::executeQuery("DELETE FROM tblMailNotification
			// 							 WHERE userFK = $userID");

			$sql = "DELETE FROM tblMailNotification WHERE userFK = ?";
			$types = "i";
			$input = array($userID);
			$del = oslDAO::executePrepared($sql, $types, $input);
			// Once deleted, get the array posted by the user.
			foreach ($input_post as $key => $val) {
				// If MN exists in the key then insert the record to the database.
				if (preg_match("/MNTaskFK/", "$key")) {
					// Insert.
					// $ins = oslDAO::executeQuery("INSERT INTO tblMailNotification
					// 							 (taskFK,userFK)
					// 							 VALUES
					// 							 ($val,$userID)");

					$sql = "INSERT INTO tblMailNotification (taskFK,userFK) VALUES (?,?)";
					$types = "ii";
					$input = array($val, $userID);
					$ins = oslDAO::executePrepared($sql, $types, $input);
				}
			}
		}
		// Return.
		return $url;
	}

	static function _dump_mail($to, $bcc, $subject)
	{
		$f = fopen("/tmp/mail.dump.txt", "a");
		fwrite($f, "$to\n$bcc\nsubject\n\n\n");
		fclose($f);
	}

	// Send mail notification.
	static function sendMail($callRep, $callTask, $appURL)
	{
		global $showTags, $sendLMail;

		if (!$sendLMail) {
			return;
		}

		// Initialise initial variables.
		$appName = oslApp::getAppInfo("appName");
		$username = str_replace(" ", ".", "osl_$appName");
		$authorName = oslContent::getReportInfo("authorNames", $callRep);
		$reportTitle = oslContent::getReportInfo("reportTitle", $callRep);
		$reportText = oslContent::getReportInfo("reportText", $callRep);
		$sectionName = oslContent::getTaskInfo("sectionName", $callTask);
		$taskName = oslContent::getTaskInfo("taskName", $callTask);
		$tagCall = oslContent::getReportTagCall($callRep);
		if ($tagCall !== FALSE && $tagCall != "") {
			$reportText = "<$tagCall>$reportText<$tagCall/>";
		}
		$bcc = oslUser::getMNRecipients($callTask, oslDAO::getReportTags($callRep));

		$domain = oslApp::getDomain();
		$tags = "";
		if ($showTags) {
			$tags = oslContent::outputReportTags($callRep, NULL);
		}
		// Initialise mail variables and paramaters.
		# $to = $username . "@" . $domain;
		$to = $bcc;

		$from = $username . "@" . $domain;
		$subject = "New message in $appName: $reportTitle";
		$contents = "<html><body><p>A new report has been made in the $appName Logbook by <strong>$authorName</strong>, with the title:</p>\r" .
			"<p><strong>$reportTitle</strong></p>\r" .
			"<p><a href=\"http://$appURL" . "index.php?callRep=$callRep\">Click here to view the report</a>.</p>\r" .
			"<hr/>" .
			$reportText .
			"<hr/>" .
			$tags .
			oslFile::displayFiles($callRep, 1, 0, 1, FALSE, 2) .
			oslFile::displayFiles($callRep, 1, 0, 0, FALSE, 2) .
			"</body></html>";

		$contents = wordwrap($contents, 70);
		// Set email parameters.
		#$params = "-f" . $from;
		// Set headers to send.

		$headers = "From:" . $from . "\r\n" .
			#"bcc:" . $bcc . "\r\n" .
			"Content-Type:text/html; charset=utf-8\r\n" .
			"Content-Transfer-Encoding:8bit\r\n\r\n";
		
		error_clear_last();
		$result = mail($to, $subject, $contents, $headers);
		
		if (!$result) {
			$var = print_r(error_get_last()['message'], true);
			oslMessages::addError("Error:".$var);
		}
		// else {
		// 	oslMessages::addNote("Sent".$bcc);
		// }
	}

	// Get mail notification recipients.
	static function getMNRecipients($callTask, $tagNames)
	{
		// Init.
		$toString = NULL;
		// Get email addresses.
		$types = "i";
		$args = array($callTask);
		$query = "SELECT email
			FROM tblMailNotification, tblUsers
			WHERE userFK = userID AND taskFK=? AND email <> ''";
		if (count($tagNames) > 0) {
			$query = "($query)
				UNION DISTINCT
				(SELECT email
			 	  FROM tblTasks, tblTags, tblMailNotification, tblUsers
				  WHERE tblTasks.taskName = tblTags.tag AND tblTasks.taskID = tblMailNotification.taskFK
				   AND tblMailNotification.userFK = tblUsers.userID
				   AND email <> ''
				   AND tblTags.tag in " . oslDAO::createListForSelect(count($tagNames)) . ")";
			foreach ($tagNames as $k => $val) {
				$types .= "s";
				$args[] = $val;
			}
		}
		$rows = oslDAO::executePrepared($query, $types, $args);
		// Build list of email addresses to return to send mail function.
		foreach ($rows as $k => $loop) {
			$email = $loop["email"];
			$toString .= $email . ",";
		}
		// Return.
		return $toString;
	}

	// Output User Management.
	static function outputUserManagement($userID, $callContent, $callUser, $tabs)
	{
		// Init.
		$str = NULL;
		// If admin.
		if (oslUser::checkIfAdmin($userID)) {
			// Add number of tabs required.
			$tabStr = oslStructure::getRequiredTabs($tabs);
			// Output title.
			$title = oslContent::getContentTitle($callContent);
			$userHdr = "Add";
			// If user details are being edited, call them here.
			if ($callUser != NULL && $callUser != 0) {
				$edUserID = oslUser::getUserInfo("userID", $callUser);
				$edUsername = oslUser::getUserInfo("username", $callUser);
				$edPassword = oslUser::getUserInfo("password", $callUser);
				$edForename = oslUser::getUserInfo("forename", $callUser);
				$edSurname = oslUser::getUserInfo("surname", $callUser);
				$edEmail = oslUser::getUserInfo("email", $callUser);
				$edGroup = oslUser::getUserInfo("userGroupFK", $callUser);
				$userHdr = "Edit";
			}
			// Output header.
			$str .= "$tabStr<p><img src=\"images/greenSquareShadowed.gif\" alt=\"\" class=\"greenSquareShadowed\" />This section enables administrators to add, edit and remove users and to decide upon access levels.</p>\n";
			// Check if access is restricted to a specific user group.
			if (oslApp::getAppInfo("appRestrict") == 1) {
				$str .= "$tabStr<p><img src=\"images/attention.gif\" alt=\"Attention\" class=\"\moveDown\" /> Access to this OSLogbook is restricted so that only users listed below have access. <a href=\"index.php?content=7\">To un-restrict access so that all authenticated users are able to access the OSLogbook, go here</a>.<p>";
			}
			// Output header.
			$str .= oslStructure::getTitleHdr("$userHdr user", $tabs);
			// Open form.
			$str .= "$tabStr<form name=\"frmUserInfo\" id=\"frmUserInfo\" method=\"post\">\n";
			// Username.
			$str .= "$tabStr<div class=\"coverDiv\">\n";
			$str .= "$tabStr	<div class=\"coverHdrDiv\">Username</div>\n";
			$str .= "$tabStr	<div class=\"coverDetailsDiv\"><input type=\"text\" name=\"username\" id=\"username\" value=\"$edUsername\" size=\"40\" /></div>\n";
			$str .= "$tabStr</div>\n";
			// Password.
			$str .= "$tabStr<div class=\"coverDiv\">\n";
			$str .= "$tabStr	<div class=\"coverHdrDiv\">Password</div>\n";
			$str .= "$tabStr	<div class=\"coverDetailsDiv\"><input type=\"text\" name=\"password\" id=\"password\" value=\"$edPassword\" size=\"40\" /></div>\n";
			$str .= "$tabStr</div>\n";
			// Forename.
			$str .= "$tabStr<div class=\"coverDiv\">\n";
			$str .= "$tabStr	<div class=\"coverHdrDiv\">Forename</div>\n";
			$str .= "$tabStr	<div class=\"coverDetailsDiv\"><input type=\"text\" name=\"forename\" id=\"forename\" value=\"$edForename\" size=\"40\" /></div>\n";
			$str .= "$tabStr</div>\n";
			// Surname.
			$str .= "$tabStr<div class=\"coverDiv\">\n";
			$str .= "$tabStr	<div class=\"coverHdrDiv\">Surname</div>\n";
			$str .= "$tabStr	<div class=\"coverDetailsDiv\"><input type=\"text\" name=\"surname\" id=\"surname\" value=\"$edSurname\" size=\"40\" /></div>\n";
			$str .= "$tabStr</div>\n";
			// Email.
			$str .= "$tabStr<div class=\"coverDiv\">\n";
			$str .= "$tabStr	<div class=\"coverHdrDiv\">Email</div>\n";
			$str .= "$tabStr	<div class=\"coverDetailsDiv\"><input type=\"text\" name=\"email\" id=\"email\" value=\"$edEmail\" size=\"40\" /></div>\n";
			$str .= "$tabStr</div>\n";
			// Access Level.
			$str .= "$tabStr<div class=\"coverDiv\">\n";
			$str .= "$tabStr	<div class=\"coverHdrDiv\">Access Level</div>\n";
			$str .= "$tabStr	<div class=\"coverDetailsDiv\">\n";
			$str .= "$tabStr		<select name=\"userGroupFK\" id=\"userGroupFK\">\n";
			// Output user groups.
			// $getUG = oslDAO::executeQuery("SELECT *
			// 			  				   FROM tblUserGroups
			// 			  				   ORDER BY userGroup DESC");

			$getUG = "SELECT * FROM tblUserGroups ORDER BY userGroup DESC";
			$types = "";
			$input = array();
			$rows = oslDAO::executePrepared($getUG, $types, $input);
			// Loop UGs.
			// while($UGLoop = mysqli_fetch_array($getUG))
			foreach ($rows as $UGLoop) {
				// Init.
				$UGID = $UGLoop["userGroupID"];
				$UG = $UGLoop["userGroup"];
				$defUG = NULL;
				// Set UG level
				if ($edGroup == $UGID) {
					$defUG = " selected";
				}
				// Ouput
				$str .= "$tabStr			<option value=\"$UGID\"$defUG>$UG</option>\n";
			}
			$str .= "$tabStr		</select>\n";
			$str .= "$tabStr	</div>\n";
			$str .= "$tabStr</div>\n";
			// Buttons.
			$str .= "$tabStr<div class=\"coverDiv\">\n";
			$str .= "$tabStr	<div class=\"coverDetailsDiv\">\n";
			$str .= oslStructure::getButton("btnSaveUserInfo", "SAVE USER INFO", "frmUserInfo", "includes/confirmation.php?adminType=" . $userHdr . "User&callUser=$edUserID", NULL, NULL, $tabs + 1);
			// Output delete if editing.
			if ($callUser != NULL) {
				$str .= oslStructure::getButton("btnDeleteUser", "DELETE USER", NULL, NULL, NULL, "requestConfirmation('Are you sure you want to delete the user $edUsername?','includes/confirmation.php?adminType=delUser&callUser=$callUser')", $tabs + 1);
			}
			$str .= "$tabStr	</div>\n";
			$str .= "$tabStr</div>\n";
			// Close form.
			$str .= "$tabStr</form>\n";
			$str .= "$tabStr<div class=\"coverDiv\">\n";
			$str .= "$tabStr<p>Current users, displayed by Access Level, are listed below:</p>\n";

			// List all current users.
			$userList = oslUser::getUserList();
			$listUserGroup = "";
			$lastUserGroup = "";
			// Loop through and output them.
			foreach ($userList as $userLoop) {
				// Init.
				$listUserID = $userLoop["userID"];
				$listUsername = $userLoop["username"];
				$listUserGroup = $userLoop["userGroup"];
				if ($listUserGroup != $lastUserGroup) {
					$str .= oslStructure::getTitleHdr($listUserGroup . "s", $tabs);
				}
				// Output.
				$str .= "$tabStr<p class=\"user\"><img src=\"images/arrow.gif\" alt=\"\" /> <a href=\"index.php?content=8&callUser=$listUserID\">$listUsername</a></p>\n";
				$lastUserGroup = $listUserGroup;
			}
			$str .= "$tabStr</div>\n";
			$str .= oslStructure::getBreak($tabs);
			// Output.
			echo oslStructure::getRoundedContentDiv("divUserManagement", $title, $str, $tabs - 2);
		}
	}

	// Kickout.
	static function kickout()
	{
		// Re-direct.
		header('location: index.php?loginError=1');
		// Make sure everything stops.
		die();
	}

	static function getUserList($orderByGroup = TRUE)
	{
		$userList = array();
		// List all current users.
		// $query = "SELECT userID,username,userGroup
		// 				  FROM tblUsers
		// 				  LEFT JOIN tblUserGroups ON tblUsers.userGroupFK = tblUserGroups.userGroupID\n";
		// $query .= "			   ORDER BY ".($orderByGroup ? "userGroup, " : "")."username";
		// $getUsers = oslDAO::executeQuery($query);

		$sql = "SELECT userID,username,userGroup FROM tblUsers
            LEFT JOIN tblUserGroups
            ON tblUsers.userGroupFK = tblUserGroups.userGroupID\n
            ORDER BY " . ($orderByGroup ? "userGroup, " : "") . "username";
		$types = "";
		$input = array();
		$rows = oslDAO::executePrepared($sql, $types, $input);
		// Loop through and output them.
		// while($userLoop = mysqli_fetch_array($getUsers))
		foreach ($rows as $userLoop) {
			// Init.
			$user = array();
			$user["userID"] = $userLoop["userID"];
			$user["username"] = $userLoop["username"];
			$user["userGroup"] = $userLoop["userGroup"];
			$userList[] = $user;
		}
		return $userList;
	}
}

?>