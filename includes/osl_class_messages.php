<?php
/*
Copyright (C) 2014, California Institute of Technology.

This file is part of the LIGO version of the OSLogbook

OSLogbook is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

This file was written by Jonathan Hanks <hanks_j@ligo-wa.caltech.edu
*/

////////////////////////////////////////
// OSLogboook Session based messages //
//////////////////////////////////////

class oslMessages
{
	static function _index($type)
	{
		return "msg_".$type;
	}

	static function _add($type, $msg)
	{
		$index = oslMessages::_index($type);
		if (!isset($_SESSION[$index])) {
			$_SESSION[$index] = array();
		}
		$_SESSION[$index][] = $msg;
	}

	static function _get($type)
	{
		$index = oslMessages::_index($type);
		$ret = array();
		if (!isset($_SESSION[$index])) {
			return $ret;
		}
		$ret = $_SESSION[$index];
		unset($_SESSION[$index]);
		return $ret;
	}

	// Add an error message
	static function addError($msg)
	{
		oslMessages::_add("err", $msg);
	}

	// Add a note
	static function addNote($msg)
	{
		oslMessages::_add("note", $msg);
	}

	static function getErrors()
	{
		return oslMessages::_get("err");
	}

	static function getNotes()
	{
		return oslMessages::_get("note");
	}


}

?>