<?php
/*
Copyright (C) 2010,  European Gravitational Observatory.

This file is part of OSLogbook.

OSLogbook is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

This file was written by Gary Hemming <gary.hemming@ego-gw.it>.
*/

///////////////////////////////
// OSLogbook structure info //
/////////////////////////////

class oslStructure
{
	// Output header and start building page.
	static function outputHeader($callContent, $userID, $getAllContent, $printCall, $callHelp)
	{
		global $authExpired, $globalNote;

		// Init.
		$str = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n";
		$str .= "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n";
		// Open head.
		$str .= "<head>\n\n";
		$str .= "	<!-- Link to stylesheets. -->\n";
		$str .= "	<link href=\"css/general.css\" rel=\"stylesheet\" type=\"text/css\" />\n";
		$str .= "	<!-- Output meta data. -->\n";
		$str .= "	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n";
		$str .= "	<!-- Disable robots. -->\n";
		$str .= "	<meta name=\"robots\" content=\"noindex,nofollow,noarchive\" />\n";
		$str .= "	<meta name=\"googlebot\" content=\"noindex,nofollow,noarchive\" />\n";
		$str .= "	<meta name=\"slurp\" content=\"noindex,nofollow,noarchive\" />\n";
		$str .= "	<meta name=\"msnbot\" content=\"noindex,nofollow,noarchive\" />\n";
		$str .= "	<meta name=\"teoma\" content=\"noindex,nofollow,noarchive\" />\n";
		$str .= "	<!-- Output title. -->\n";
		$str .= "	<title>" . oslApp::getAppInfo("appName") . " Logbook</title>\n";
		$str .= "	<!-- Call general Javascripts. -->\n";
		$str .= "	<script type=\"text/javascript\" src=\"scripts/ajax.js\"></script>\n";
		$str .= "	<script type=\"text/javascript\" src=\"scripts/standardFunctions.js\"></script>\n";
		$str .= "	<!-- Call specific Javascripts. -->\n";
		$str .= "	<script type=\"text/javascript\" src=\"scripts/editSection.js\"></script>\n";
		$str .= "	<script type=\"text/javascript\" src=\"scripts/redirect.js\"></script>\n";
		$str .= "	<script type=\"text/javascript\" src=\"scripts/updateTasks.js\"></script>\n";

		// link in the rss feed if we are called via an iframe
		if ($getAllContent and $callContent == 1) {
			$str .= "   <link rel=\"alternate\" type=\"application/rss+xml\" href=\"rss-feed.php\"/>\n";
		}
		// Set base target in iframe to parent.
		$str .= "	<!-- Set base target. -->\n";
		$str .= "	<base target=\"_top\" />\n\n";
		// Close head.
		$str .= "</head>\n\n";
		// If calling print.
		if ($printCall == TRUE) {
			$jsCall = " onLoad=\"javascript:window.print()\"";
		} else {
			$jsCall = "";
		}
		// Set body to white background if called via iframe.
		if ($getAllContent == FALSE) {
			$class = " class=\"white\"$jsCall";
		} else {
			$class = "";
		}
		// Open body.
		$str .= "<body$class>\n\n";
		// If calling all content.
		if ($getAllContent == TRUE) {
			// Open shell div.
			$str .= oslStructure::openDiv("shell", 1, NULL);
			// Open main div.
			$str .= oslStructure::openDiv("main", 2, NULL);
			// Open upper div.
			$str .= oslStructure::openDiv("upper", 3, NULL);
			// Open logo div.
			$str .= oslStructure::openDiv("logo", 4, NULL);
			// Output header.
			$str .= "					<!-- #Output header. -->\n";
			$str .= "					<p class=\"headerTxt\">" . oslApp::getAppInfo("appName") . " Logbook<br/>\n";
			#$str .= "              <span class=\"smaller\">Logbooks <a href=\"https://alog.ligo-wa.caltech.edu/aLOG/\">LHO</a> <a href=\"https://alog.ligo-la.caltech.edu/aLOG/\">LLO </a><a href=\"https://logbook.virgo-gw.eu/virgo/\">Virgo</a> <a href=\"https://klog.icrr.u-tokyo.ac.jp/osl/\">KAGRA</a></span></p>\n";
			// Close logo div.
			$str .= oslStructure::closeDiv("logo", 4);
			// Open quickSearch div.
			$str .= oslStructure::openDiv("quickSearch", 4, NULL);
			// Get quick search form.
			$str .= oslStructure::getQuickSearchForm(5);
			// Close quickSearch div.
			$str .= oslStructure::closeDiv("quickSearch", 4);
			// Open login div.
			$str .= oslStructure::openDiv("loginDiv", 4, NULL);
			// Output quick search.
			$str .= oslStructure::getLoginForm($userID, 5);
			// Close login div.
			$str .= oslStructure::closeDiv("loginDiv", 4);
			// Close upper div.
			$str .= oslStructure::closeDiv("upper", 3);
			// Open links div.
			$str .= oslStructure::openDiv("links", 3, NULL);
			// Get top links.
			$str .= oslStructure::outputTopLinks($callContent, $userID);
			// Close links div.
			$str .= oslStructure::closeDiv("links", 3);
			// Open borderBottom div.
			$str .= oslStructure::openDiv("borderBottom", 3, NULL);
			// Close borderBottom div.
			$str .= oslStructure::closeDiv("borderBottom", 3);
		} else {
			if ($authExpired) {
				echo "<p>Your session has expired.  Any posts you were working on have been saved as drafts.  No deletions or administrative actions have happened.</p><p>Please log-in again to continue.</p>\n";
			}
			if ($globalNote !== NULL) {
				echo $globalNote;
			}
		}
		// Return.
		return $str;
	}

	// Open iframe.
	static function getMainIframe($callContent, $tabs)
	{
		// Get variables set elsewhere.
		global $callRep, $preview, $startPage, $printCall, $findRep, $step, $callUser, $addCommentTo, $callHelp, $callFileType, $authExpired;
		// Add number of tabs required.
		$tabStr = oslStructure::getRequiredTabs($tabs);
		// Get iframe.
		$str = "$tabStr<!--- Output main iframe -->\n";
		$str .= "$tabStr<iframe class=\"iframeReport\" name=\"ifrMain\" id=\"ifrMain\" onload=\"autoResizeIframe()\" height=\"70%\" frameborder=\"0\" scrolling=\"yes\" src=\"iframeSrc.php?authExpired=$authExpired&content=$callContent&step=$step&callRep=$callRep&startPage=$startPage&preview=$preview&printCall=$printCall&callUser=$callUser&addCommentTo=$addCommentTo&callHelp=$callHelp&callFileType=$callFileType#$findRep\" tabindex=\"-1\" />\n\n";
		// Return.
		return $str;
	}

	// Open div.
	static function openDiv($name, $tabs, $class)
	{
		// Init.
		$str = NULL;
		// Set class.
		if ($class != NULL) {
			$class = " class=\"$class\"";
		}
		// Add number of tabs required.
		$tabStr = oslStructure::getRequiredTabs($tabs);
		// Set comment.
		$str .= "$tabStr<!--- Output $name div -->\n";
		// Open div.
		$str .= "$tabStr<div id=\"$name\"$class>\n\n";
		// Return.
		return $str;
	}

	// Close div.
	static function closeDiv($name, $tabs)
	{
		// Init.
		$str = NULL;
		// Add number of tabs required.
		$tabStr = oslStructure::getRequiredTabs($tabs);
		// Add break.
		$str .= oslStructure::getBreak($tabs + 1);
		// Set comment.
		$str .= "$tabStr<!--- Close $name div -->\n";
		// Close div.
		$str .= "$tabStr</div>\n\n";
		// Return.
		return $str;
	}

	// Get required tabs.
	static function getRequiredTabs($tabs)
	{
		$str = NULL;
		// Add number of tabs required.
		for ($a = 0; $a < $tabs; $a++) {
			$str .= "	";
		}
		// Return.
		return $str;
	}
	// Output footer.
	static function outputFooter($getAllContent)
	{
		// If calling all content.
		if ($getAllContent == TRUE) {
			// Close shell div.
			echo oslStructure::closeDiv("main", 2);
			// Close main div.
			echo oslStructure::closeDiv("shell", 1);
		}
		// Close body.
		echo "\n</body>\n\n";
		echo "</html>\n";
	}

	// Get a button.
	static function getButton($id, $txt, $form, $way, $direction, $callJS, $tabs)
	{
		// Init.
		$str = NULL;
		// Handle direction order.
		$defDir = "Left";
		if ($direction != NULL) {
			$defDir = $direction;
		}
		// Decide which Javascript to use.
		if ($callJS != NULL) {
			$js = $callJS;
		}
		// Otherwise, set to redirect.
		else {
			$js = "redirect('$form','$way')";
		}
		// Incorporate Javascript in onclick.
		$js = "onclick=\"$js\"";
		// Add number of tabs required.
		$tabStr = oslStructure::getRequiredTabs($tabs);
		// Set the button.
		$str .= "$tabStr<!-- Open $txt button. -->\n";
		$str .= "$tabStr<div class=\"buttonContainer$defDir\" id=\"" . $id . "container\" $js>\n";
		$str .= "$tabStr	<!-- Output button arrow. -->\n";
		$str .= "$tabStr	<img src=\"images/buttonLeft.gif\" class=\"buttonLeft\" alt=\"\" />\n";
		$str .= "$tabStr	<!-- Output button text. -->\n";
		$str .= "$tabStr	<div id=\"$id\" class=\"buttonRight\">$txt</div>\n";
		$str .= "$tabStr<!-- Close $txt button. -->\n";
		$str .= "$tabStr</div>\n\n";
		return $str;
	}

	// Get a break.
	static function getBreak($tabs)
	{
		// Add number of tabs required.
		$tabStr = oslStructure::getRequiredTabs($tabs);
		// Build break.
		$break = "$tabStr<!-- Output break div. -->\n" .
			"$tabStr<div class=\"break\"></div>\n\n";
		// Return break.
		return $break;
	}

	// Incorporate text in title header.
	static function getTitleHdr($txt, $tabs)
	{
		// Init.
		$str = NULL;
		// If txt sent.
		if ($txt != NULL) {
			// Add number of tabs required.
			$tabStr = oslStructure::getRequiredTabs($tabs);
			// Set divs.
			$str = "$tabStr<div class=\"titleTxtLeftBg\"></div>\n" .
				"$tabStr<div class=\"titleTxt\">$txt</div>\n" .
				"$tabStr<div class=\"titleTxtRightBg\"></div>\n" .
				oslStructure::getBreak($tabs);
		}
		// Return.
		return $str;
	}

	// Output login form.
	static function getLoginForm($callUser, $tabs)
	{
		global $showLoginButtons;
		// Init.
		$str = NULL;
		// Add number of tabs required.
		$tabStr = oslStructure::getRequiredTabs($tabs);
		// If logged-in output user details and log-out options.
		if ($callUser != NULL && $callUser != 0) {
			if ($showLoginButtons) {
				// Get button.
				$str .= oslStructure::getButton("btnLogOut", "LOG-OUT", NULL, "includes/logout.php", "Right", NULL, $tabs);
				// Set text.
			}
			$str .= "$tabStr<!-- Output logged-in text. -->\n";
			$str .= "$tabStr<div id=\"loginTxt\">" . oslUser::getUserInfo("username", $callUser) . "</div>\n";
		}
		// Otherwise, open log-in form.
		else {
			if ($showLoginButtons) {
				$str .= "$tabStr<!-- Open log-in form. -->\n";
				$str .= "$tabStr<form name=\"login\" id=\"login\" method=\"post\">\n";
				$str .= "$tabStr<input type=\"hidden\" name=\"target\" value=\"" . "https://" . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"] . "\" />\n";
				if (oslApp::getAppInfo("appLoginMethod") == 3) {
					$str .= oslStructure::getButton("btnLogIn", "LOG-IN", "login", "https://" . $_SERVER["SERVER_NAME"] . "/Shibboleth.sso/Login", "Right", NULL, $tabs);
				} else {
					$str .= oslStructure::getButton("btnLogIn", "LOG-IN", "login", "includes/login.php", "Right", NULL, $tabs);
					$str .= oslStructure::getInputBox("password", "password", "10", "", " onfocus='clearInputBox(this)' ", "password", "login", $tabs, 2);
					$str .= oslStructure::getInputBox("username", "text", "10", "", " onfocus='clearInputBox(this)' ", "username", "login", $tabs, 1);
				}
				// Close form.
				$str .= "$tabStr<!-- Close log-in form. -->\n";
				$str .= "$tabStr</form>\n";
			}
		}
		// Return.
		return $str;
	}

	// Output quick search form.
	static function getQuickSearchForm($tabs)
	{
		$reportSection = NULL;
		$reportTask = NULL;
		// Add number of tabs required.
		$tabStr = oslStructure::getRequiredTabs($tabs);
		// Output.
		$str = "$tabStr<!-- Open Quick Task Search -->\n";
		$str .= "$tabStr<div class=\"quickSearchContainer\">\n";
		$str .= "$tabStr<form name=\"searchTaskFrm\" id=\"searchTaskFrm\" method=\"post\">\n";

		$str .= oslStructure::getButton("btnTaskSearch", "SEARCH TASKS", "searchTaskFrm", "includes/search.php?adminType=search", "Right", NULL, $tabs + 1);
		$str .= "$tabStr		<div class=\"quickSearchContainer\">\n";

		$str .= "$tabStr		<select name=\"reportSection\" id=\"reportSection\" onkeydown=\"processEnterKey(event, 'btnTaskSearchcontainer')\" onchange=\"updateTasks(1)\">\n";
		// Output blank.
		$str .= "$tabStr			<option value=\"\"></option>\n";
		// Get sections.
		$rows = oslDAO::executePrepared("SELECT * FROM tblSections ORDER BY sectionName", "", array());
		// Loop sections.
		foreach ($rows as $loop) {
			// Init.
			$sectionID = $loop["sectionID"];
			$sectionName = $loop["sectionName"];
			$sel = NULL;
			if ($reportSection == $sectionID) {
				$sel = " selected";
			}
			// Output options.
			$str .= "$tabStr			<option value=\"$sectionID\"$sel>$sectionName</option>\n";
		}
		$str .= "$tabStr		</select>\n";
		$str .= "$tabStr		<select name=\"reportTask\" id=\"reportTask\" onkeydown=\"processEnterKey(event, 'btnTaskSearchcontainer')\">\n";
		$str .= "$tabStr			<option value=\"\"></option>\n";
		// If a section has been selected, get all related tasks
		if ($reportSection != NULL) {
			// Get tasks.
			$rows = oslDAO::executePrepared("SELECT * FROM tblTasks WHERE sectionFK=? ORDER BY taskName", "i", array($reportSection));
			// Loop tasks.
			foreach ($rows as $loop) {
				// Init.
				$taskID = $loop["taskID"];
				$taskName = $loop["taskName"];
				$sel = NULL;
				if ($reportTask == $taskID) {
					$sel = " selected";
				}
				// Output options.
				$str .= "$tabStr			<option value=\"$taskID\"$sel>$taskName</option>\n";
			}
		}
		$str .= "$tabStr		</select>\n";
		$str .= "$tabStr		</div>\n";

		$str .= "$tabStr<!-- Close Quick Task Search  -->\n";
		$str .= "$tabStr</form>\n";
		$str .= "$tabStr</div>\n\n";

		$str .= "$tabStr<div class=\"break\"></div>\n\n";

		$str .= "$tabStr<!-- Open Quick Search form. -->\n";
		$str .= "$tabStr<div class=\"quickSearchContainer\">\n";
		$str .= "$tabStr<form name=\"searchFrm\" id=\"searchFrm\" method=\"post\">\n";
		$str .= oslStructure::getButton("btnQuickSearch", "SEARCH", "searchFrm", "includes/search.php?adminType=quickSearch", "Right", NULL, $tabs + 1);
		$str .= oslStructure::getInputBox("srcKeyword", "text", "20", "", "onkeydown=\"processEnterKey(event, 'btnQuickSearchcontainer')\" onclick=\"clearInputBox(this)\"", "Quick search", "login", $tabs + 1, 0);
		$str .= oslStructure::getInputBox("srcKeywordType", "hidden", "", "", "", "2", "", $tabs + 1, 0);

		$str .= "$tabStr<!-- Close Quick Search form. -->\n";
		$str .= "$tabStr</form>\n";
		$str .= "$tabStr</div>\n\n";

		// Return.
		return $str;
	}
	// Get border break.
	static function getBorderBreak()
	{
		// Set.
		$str .= "<div id=\"qsBorderBreak\" class=\"breakBorder\"></div>\n";
		// Return.
		return $str;
	}
	// Output the links at the top of the page.
	static function outputTopLinks($callContent, $callUser)
	{
		// Init.
		$str = NULL;
		// If user has permission.
		if (
			oslApp::getAppInfo("appOpen") == 1
			|| (oslApp::getAppInfo("appOpen") == 0 && $callUser != NULL)
		) {
			// Get contents to be output in top links.
			$rows = oslDAO::executePrepared("SELECT * FROM tblContents ORDER BY orderPos", "", array());
			// Loop through and output top links.
			foreach ($rows as $loop) {
				// Initialise variables.
				$contentID = $loop["contentID"];
				if (oslUser::checkContentAccess($callUser, $contentID)) {
					$contentName = $loop["contentName"];
					// If this content is currently selected output the selected Class.
					$class = oslContent::checkContent($callContent, $contentID);
					// If this is Drafts check whether the user currently has any saved and output the total if they have.
					if ($contentID == 6) {
						$totalDrafts = oslContent::countDrafts($callUser);
						if ($totalDrafts != 0) {
							$contentName .= " ($totalDrafts)";
						}
					}
					// Output top links
					$str .= "				<a href=\"index.php?content=$contentID\" class=\"opt$class\">$contentName</a>\n";
				}
			}
		}
		// Return.
		return $str;
	}

	// Output input box.
	static function outputInputBox($box, $type, $length, $imgSrc, $jsCall, $defaultVal, $class, $tabs, $tabIndex)
	{
		// Get what is called.
		echo oslStructure::getInputBox($box, $type, $length, $imgSrc, $jsCall, $defaultVal, $class, $tabs, $tabIndex);
	}

	// Get input box.
	static function getInputBox($box, $type, $length, $imgSrc, $jsCall, $defaultVal, $class, $tabs, $tabIndex)
	{
		// If an image output the src.
		if ($imgSrc != NULL) {
			$src = " src=\"images/$imgSrc\" ";
		} else {
			$src = $imgSrc;
		}
		// If calling a Javascript output it here.
		if ($jsCall != NULL) {
			// If onfocus command sent.
			if (preg_match("/onfocus|onkeydown/", $jsCall)) {
				$js = $jsCall;
			}
			// Otherwise, incorporate in onclick.
			else {
				$js = " onclick=\"$jsCall\" ";
			}
		} else {
			$js = "";
		}

		// If a tabindex has been set.
		if ($tabIndex != NULL) {
			$tbI = " tabindex=\"$tabIndex\" ";
		} else {
			$tbI = $tabIndex;
		}
		// Add number of tabs required.
		$tabStr = oslStructure::getRequiredTabs($tabs);
		// Build.
		$str = "$tabStr<!-- Output $box input box. -->\n";
		$str .= "$tabStr<input name=\"$box\" id=\"$box\" type=\"$type\" $src value=\"$defaultVal\" size=\"$length\" $js class=\"$class\" $tbI />\n";
		// Return.
		return $str;
	}

	// Get a rounded content div.
	static function getRoundedContentDiv($name, $title, $content, $tabs)
	{
		// Add number of tabs required.
		$tabStr = oslStructure::getRequiredTabs($tabs);
		// Init.
		$div = NULL;
		// Set.
		$div .= "$tabStr<!-- Output $name rounded content div. -->\n";
		$div .= "$tabStr<!-- Output title. -->\n";
		$div .= "$tabStr<div class=\"t\"><div class=\"b\"><div class=\"rHdr\"><div class=\"l\">\n";
		$div .= "$tabStr	<div class=\"blHdr\"><div class=\"brHdr\"><div class=\"tl\"><div class=\"tr\">\n";
		$div .= "$tabStr		$title\n";
		$div .= "$tabStr	</div></div></div></div>\n";
		$div .= "$tabStr</div></div></div></div>\n\n";
		$div .= "$tabStr<!-- Output content. -->\n";
		$div .= "$tabStr<div class=\"tCon\"><div class=\"bCon\"><div class=\"r\"><div class=\"l\">\n";
		$div .= "$tabStr	<div class=\"bl\"><div class=\"br\"><div class=\"tlCon\"><div class=\"trCon\">\n\n";
		$div .= "$content\n\n";
		$div .= "$tabStr	<!-- Close $name rounded content div. -->\n";
		$div .= "$tabStr	</div></div></div></div>\n";
		$div .= "$tabStr</div></div></div></div>\n\n";
		// Return.
		return $div;
	}

	// Output report title with specific header.
	static function setReportHdr($colour)
	{
		$isIE = strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE;

		// Output styles.
		echo "	<!-- Set reportSection$colour style -->\n";
		echo "	<style>\n";
		echo "		.reportSection$colour\n";
		echo "		{\n";
		echo "			position: relative;\n";
		echo "			width: " . ($isIE ? "100" : "99") . "%;\n";
		echo "			min-height: 17px;\n";
		echo "			padding: 2px 0.5%;\n";
		echo "			font-weight: bold;\n";
		echo "			background: #$colour;\n";
		echo "		}\n";
		echo "	</style>\n";
	}

	// Build keyword array.
	static function getColour($key)
	{
		if ($key == 0) {
			$colour = "red";
		} elseif ($key == 1) {
			$colour = "green";
		} elseif ($key == 2) {
			$colour = "blue";
		} elseif ($key == 3) {
			$colour = "cyan";
		} elseif ($key == 4) {
			$colour = "yellow";
		} elseif ($key == 5) {
			$colour = "purple";
		} elseif ($key == 6) {
			$colour = "orange";
		} elseif ($key == 7) {
			$colour = "aqua";
		} elseif ($key == 8) {
			$colour = "fuchsia";
		} elseif ($key == 9) {
			$colour = "lime";
		} elseif ($key == 10) {
			$colour = "maroon";
		} elseif ($key == 11) {
			$colour = "navy";
		} elseif ($key == 12) {
			$colour = "olive";
		} elseif ($key == 13) {
			$colour = "silver";
		} elseif ($key > 13) {
			$colour = "teal";
		}
		return $colour;
	}

	// Set new section colour.
	static function setNewSectionColour()
	{
		// Set array.
		$randArray = array('33', '66', '99', 'CC', 'FF');
		// Get individual bricks.
		$colA = array_rand($randArray);
		$colB = array_rand($randArray);
		$colC = array_rand($randArray);
		// Build colour from bricks.
		$colour = $randArray[$colA] . $randArray[$colB] . $randArray[$colC];
		// Return new colour.
		return $colour;
	}

	// Add leading zeroes.
	static function leadingZeroes($number, $zeroes)
	{
		// Get current length of string.
		$strLen = strlen($number);
		// Subtract strLen from zeroes.
		$zeroesReq = $zeroes - $strLen;
		for ($a = $strLen; $a < $zeroes; $a++) {
			$number = "0" . $number;
		}
		return "$number";
	}

	// Output sections as select options.
	static function outputSectionsAsOpt($key_field, $val_field)
	{
		// Init.
		$str = NULL;
		// Get.
		$rows = oslDAO::executePrepared("SELECT * from tblSections ORDER BY sectionName", "", array());
		// Loop.
		foreach ($rows as $loop) {
			// Init.
			$optKey = $loop["$key_field"];
			$optVal = $loop["$val_field"];
			// Output options.
			$str .= "				<option value=\"$optKey\">$optVal</option>\n";
		}
		// Return.
		return $str;
	}

	// Convert date to mysql string-friendly.
	static function mysqlDateConversion($date, $inc)
	{
		// Init.
		$day = $date[0] . $date[1];
		$month = $date[3] . $date[4];
		$year = $date[6] . $date[7] . $date[8] . $date[9];
		// Increment date if necessary.
		if ($inc != NULL) {
			$day = $day + $inc;
		}
		// Format.
		$formattedDate = "$year-$month-$day";
		// Return.
		return $formattedDate;
	}

	// Get the user's default editor.
	static function getEditor($callUser, $default, $callEditor, $tabs)
	{
		// Init.
		$str = NULL;
		// If user called.
		if ($callUser != 0 && $callUser != NULL) {
			// Add number of tabs required.
			$tabStr = oslStructure::getRequiredTabs($tabs);
			// If editing.
			if ($callEditor != NULL) {
				$def = $callEditor;
			} else {
				// Get defaultEditor.
				$def = oslUser::getUserInfo("defaultEditor", $callUser);
			}
			// If ASCII text.
			if ($def == 1) {
				$str .= "$tabStr<textarea name=\"editor1\" id=\"editor1\" class=\"ckeditorTA\">$default</textarea>\n";
			}
			// If CKEditor.
			elseif ($def == 2) {
				$str .= oslStructure::getCkEditor($default, $tabs);
			}
		}
		// Return.
		return $str;
	}

	// Output ckeditor.
	static function getCkEditor($default, $tabs)
	{
		$cke = "";

		// Get the script name.
		$loc = str_replace("iframeSrc.php", "", $_SERVER["SCRIPT_NAME"]);

		// Add number of tabs required.
		$tabStr = oslStructure::getRequiredTabs($tabs);

		// Set default textarea.
		$cke .= "$tabstr	<script type=\"text/javascript\" src=\"scripts/ckeditor/ckeditor.js\"></script>\n";
		$cke .= "$tabStr	<textarea class=\"ckeditorTA\" id=\"editor1\" name=\"editor1\">$default</textarea>\n";
		$cke .= "$tabStr	<script type=\"text/javascript\">\n";
		// Invoke ckeditor.
		$cke .= "$tabStr		CKEDITOR.replace( 'editor1', \n";
		$cke .= "$tabStr		{\n";
		$cke .= "$tabStr			height : '400px',\n";
		$cke .= "$tabStr			customConfig : '" . $loc . "scripts/ckcustom/osl_config.js',\n";
		$cke .= "$tabStr			toolbar : 'oslToolbar'\n";
		$cke .= "$tabStr		});\n";
		$cke .= "$tabStr	</script>\n";
		// Return.
		return $cke;
	}

	// Get Tigra calendar.
	static function getTigraCalendar($tabs, $frm, $input)
	{
		$str = "";

		// Add number of tabs required.
		$tabStr = oslStructure::getRequiredTabs($tabs);

		// Initialise calendar instance.
		$str .= "$tabStr<link href=\"scripts/tigra/calendar.css\" rel=\"stylesheet\" type=\"text/css\" />\n";
		$str .= "$tabStr<script type=\"text/javascript\" src=\"scripts/tigra/calendar_eu.js\"></script>\n";
		$str .= "$tabStr<script language=\"JavaScript\">\n";
		$str .= "$tabStr 	new tcal ({\n";
		$str .= "$tabStr 		'formname': '$frm',\n";
		$str .= "$tabStr 		'controlname': '$input'\n";
		$str .= "$tabStr 	});\n";
		$str .= "$tabStr</script>\n";
		// Return.
		return $str;
	}
}

?>