<?php
/*
Copyright (C) 2010,  European Gravitational Observatory.

This file is part of OSLogbook.

OSLogbook is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

This file was written by Gary Hemming <gary.hemming@ego-gw.it>.
*/
// Initialise general variables.
ob_start();
require_once ("initVar.php");

// Overwrite display errors.
ini_set("display_errors", 0);
$error = "";


// Authenticate user.
if (oslUser::authenticateUser($username, $password)) {
	// Set the userID session.
	$_SESSION["user" . $callDB . "ID"] = oslUser::getUserDetailFromUsername($username, "userID");
} else {
	$_SESSION["loginFailed"] = 1;
	// Set login error.
	$error = "?loginError=2";
}
// Return user to homepage.
header("Location: ../index.php$error");
exit;
?>