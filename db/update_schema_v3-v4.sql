--
-- Table structure for table `tblSchemaVersion`
--

DROP TABLE IF EXISTS `tblSchemaVersion`;
SET @saved_cs_client    = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `tblSchemaVersion` (
	`version` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `tblSchemaVersion`
--

INSERT INTO `tblSchemaVersion` VALUES (4);

ALTER TABLE `tblTags` ADD `type` INT(11) NOT NULL DEFAULT '0' AFTER `tag`;

INSERT INTO `tblTags` (tag, type) VALUES ('sys:Robot', 1);
INSERT INTO `tblTags` (tag, type) VALUES ('sys:HasAttachments', 1);
INSERT INTO `tblTags` (tag, type) VALUES ('sys:HasImages', 1);

ALTER TABLE `tblReports` ADD `lastCommentDate` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `dateAdded`;

UPDATE `tblReports` SET `lastCommentDate` = `dateAdded`;

UPDATE `tblHelp` SET `helpTxt` = 'Four buttons are available to users when entering or editing a Report:\r\n\r\n- Save To Draft - This saves the Report as it is in the current state, allowing a user to either continue adding/editing or to return to the Report at a later date, via the Drafts section.\r\n\r\n- Upload / Manage Files - Opens the Upload Files area, from where users can attach and remove files associated with a Report.\r\n\r\n- Preview - Shows the Report as it would appear in the list of reports available in the Home section. The only difference being that the Report is shown with a red background.\r\n\r\nPost To Logbook - Clicking on this button makes the Report visible to other uses in the list of Reports on the homepage. At this point it can no longer be edited by any non-administration level user.' WHERE helpID = 9 LIMIT 1;
