ALTER TABLE `tblReports` ADD `editorFK` INT NOT NULL DEFAULT '1' AFTER `parentFK` ;

ALTER TABLE `tblUsers` ADD `defaultEditor` INT NOT NULL DEFAULT '1';

DROP TABLE IF EXISTS `tblHelp`;

--
-- Table structure for table `tblHelp`
--

CREATE TABLE `tblHelp` (
 `helpID` int(11) NOT NULL auto_increment,
 `helpParentFK` int(11) NOT NULL default '0',
 `helpTitle` text NOT NULL,
 `helpTxt` text NOT NULL,
 `helpImgA` text NOT NULL,
 `helpRefLetter` text NOT NULL,
 PRIMARY KEY  (`helpID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tblHelp`
--

INSERT INTO `tblHelp` VALUES (1, 0, 'About the OSLogbook', '', '', 'A');
INSERT INTO `tblHelp` VALUES (2, 0, 'Viewing reports', '', '', 'B');
INSERT INTO `tblHelp` VALUES (3, 0, 'Adding/Editing reports', '', '', 'C');
INSERT INTO `tblHelp` VALUES (4, 3, 'What are the ''Editors''?', 'The Editors are the different interfaces available within the OSLogbook, through which the user can enter the details of a report.\r\n\r\nAt present, two different editors are available:\r\n\r\n- ASCII Text - This enables the user to enter a simple text report, without the use of any HTML. Reports entered using this Editor will maintain line feeds and whitespace in the same way as when they are entered.\r\n\r\n- CKEditor - This powerful interface allows users to enter reports which incorporate a range of different HTML features, without the need to know any HTML. The Editor enables tasks such as: copying and pasting; lists; table insertion; link insertion and much, much more. Full details on this open source feature can be found on the CKEditor website - <a href="http://ckeditor.com" target="_blank">ckeditor.com</a>.', '', '');
INSERT INTO `tblHelp` VALUES (5, 3, 'What is the Title?', 'The Title is a brief, overall description of the contents of the report.', '', '');
INSERT INTO `tblHelp` VALUES (6, 3, 'What is the Section?', 'The Section is the general area or theme to which the Report belongs. Each section contains at least one Task.', '', '');
INSERT INTO `tblHelp` VALUES (7, 3, 'What is the Task?', 'Each Report must be associated to a specific Task, which describes the specific field in which the Report belongs. The Task itself is associated to the more general, over-arching Section.', '', '');
INSERT INTO `tblHelp` VALUES (8, 3, 'Who are/is the Author(s)?', 'The Author is the person who submitted the Report.', '', '');
INSERT INTO `tblHelp` VALUES (9, 3, 'What do the different buttons do?', 'Four buttons are available to users when entering or editing a Report:\r\n\r\n- Save To Draft - This saves the Report as it is in the current state, allowing a user to either continue adding/editing or to return to the Report at a later date, via the Drafts section.\r\n\r\n- Upload Files - Opens the Upload Files area, from where users can attach files to a Report.\r\n\r\n- Preview - Shows the Report as it would appear in the list of reports available in the Home section. The only difference being that the Report is shown with a red background.\r\n\r\nPost To Logbook - Clicking on this button makes the Report visible to other uses in the list of Reports on the homepage. At this point it can no longer be edited by any non-administration level user.', '', '');
INSERT INTO tblHelp (helpParentFK, helpTitle, helpTxt, helpImgA, helpRefLetter) VALUES (2, 'RSS Feed', 'An RSS feed for the aLOG is available at <href="rss-feed.php">rss-feed.php</a>.', '', '');

--
-- Table structure for table `tblValues`
--

CREATE TABLE `tblValues` (
 `valueID` int(11) NOT NULL auto_increment,
 `valueGroupFK` int(11) NOT NULL default '0',
 `value` text NOT NULL,
 `tagCall` text NOT NULL,
 PRIMARY KEY  (`valueID`),
 KEY `valueGroupFK` (`valueGroupFK`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tblValues`
--

INSERT INTO `tblValues` VALUES (1, 1, 'ASCII Text', 'pre');
INSERT INTO `tblValues` VALUES (2, 1, 'CKEditor (HTML)', '');

--
-- Table structure for table `tblValueGroups`
--

CREATE TABLE `tblValueGroups` (
 `valueGroupID` int(11) NOT NULL auto_increment,
 `valueGroup` text NOT NULL,
 PRIMARY KEY  (`valueGroupID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tblValueGroups`
--

INSERT INTO `tblValueGroups` VALUES (1, 'Editor instances');
