<?php
/*
Copyright (C) 2010,  European Gravitational Observatory.

This file is part of OSLogbook.

OSLogbook is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

This file was written by Gary Hemming <gary.hemming@ego-gw.it>.
*/
// changed from .css to .php for a hack around a problem with ie.  hopefully short term
header('Content-type: text/css');

/* Detect the presence of internet explorer.  There are a few changes needed to
  properly render in ie. JKH 2011-01-06 */
function isIE()
{
    $ie = strpos($_SERVER['HTTP_USER_AGENT'],'MSIE') !== FALSE;
    //$opera = strpos($_SERVER['HTTP_USER_AGENT'],'Opera') !== FALSE;
    return $ie;
}
?>
/*
Copyright (C) 2010,  European Gravitational Observatory.

This file is part of OSLogbook.

OSLogbook is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

This file was written by Gary Hemming <gary.hemming@ego-gw.it>.
*/


/* === DIVs === */

/* === Shell container. === */
#shell
{
	position: absolute;
	min-width: 900px;
	top: 0px;
	bottom: 0px;
	right: 20px;
	left: 20px;
	background: #FFF url(../images/default-theme/left_border.png) repeat-y top left;
}

/* === Main container. === */
#main
{
	position: absolute;
	top: 0px;
	bottom: 0px;
	right: 0;
	left: 18px;
	padding: 0 18px 0 0;
	background: transparent url(../images/default-theme/right_border.png) repeat-y top right;
}

/* === Upper container. === */
#upper
{
	position: relative;
	width: 100%;
	min-height: 50px;
	height: 50px;
	background: #000 url(../images/default-theme/header_gradient.png) repeat-x top left;
}
/* === Logo container. === */
#logo
{
	position: relative;
	float: left;
	width: 25%;
	min-height: 50px;
	height: 50px;
	text-align: left;
}
/* === Quick Search & Login containers. === */
#quickSearch
{
	position: relative;
	float: left;
	width: 45.0%;
	min-height: 37px;
	height: 37px;
	padding: 2px 0.1% 0 0;
}

.quickSearchContainer
{
	position: relative;
	float: right;
}

#loginDiv
{
	position: relative;
	float: left;
	width: 29%;
	min-height: 37px;
	height: 37px;
	padding: 13px 0.1% 0 0;
}

#loginTxt
{
	position: relative;
	float: right;
	text-align: right;
	color: #FFF;
	padding: 2px 0 0 0;
    font-weight: bold;
}
/* === Messaging Framework === */
.msgNote
{
	border: 1px solid #FFCC33;
	border-radius: 5px;
	background-color: #FFFF66;
	color: black;
	width: 50%;
	padding: .15cm;
	margin: .25cm auto .25cm auto;
}
.msgError
{
	border: 2px solid #990000;
	border-radius: 5px;
	background-color: #CC6666;
	color: white;
	width: 50%;
	padding: .15cm;
	margin: .25cm auto .25cm auto;
}
/* === Login boxes. === */
.loginBox
{
	position: relative;
	float: left;
	min-height: 20px;
	padding: 0 0 0 1%;
}
.loginBoxInner
{
	position: relative;
	float: right;
}

/* === Links container. === */
#links
{
	position: relative;
	width: 100%;
	min-height: 30px;
	height: 30px;
	border: solid #FFF;
	border-width: 1px 0;
	background: #555 url(../images/default-theme/light_gradient.png) repeat-x top left;
}
/* === Border bottom container. === */
#borderBottom
{
	position: relative;
	width: 100%;
	height: 7px;
	background: url(../images/default-theme/border_bottom.png) repeat-x top left;
}
/* === Title Bar container. === */
#titleBar
{
	position: relative;
	width: 100%;
	min-height: 35px;
	height: 35px;
}
/* === Title Bar text container. === */
.titleTxt
{
	float: left;
	height: 28px;
	color: #FFF;
	padding: 4px 1px 3px 1px;
	background: url(../images/blue_background.png) repeat-x top left;
}
/* === Title text left bg container. === */
.titleTxtLeftBg, .titleTxtRightBg
{
	float: left;
	width: 11px;
	height: 35px;
	background: url(../images/blue_left.png) no-repeat top left;
}
.titleTxtRightBg
{
	width: 13px;
	background: url(../images/blue_right.png) no-repeat top left;
}

/* === Report containers === */
.reportShell
{
	position: relative;
	width: 100%;
	min-height: 20px;
	border: solid #D7D6D6;
	border-width: 1px 1px 0 1px;
}
.reportBottomLeft
{
	position: relative;
	float: left;
	width: 50%;
	height: 14px;
  	background: url(../images/blRep.gif) no-repeat top left;
}
.reportBottomRight
{
	position: relative;
	float: left;
        width: 50%;
<?php if (!isIE()) { ?>
        right: -2px;
<?php } ?>
	height: 14px;
  	background: url(../images/brRep.gif) no-repeat top right;
}
.reportBottomLine
{
	position: relative;
	height: 14px;
	width: 100%;
  	background: url(../images/miniBorderPixelShadowedBelow.gif) 0 100% repeat-x;
}
.reportDetails, .reportDetailsPreview, .authorDetails, .authorDetailsPreview, .commentBlock, .commentHdr, .commentAuthor, .comment
{
	position: relative;
	width: <?php echo (isIE() ? "100" : "99"); /* IE needs 100% not 99% */?>%;
	min-height: 17px;
	padding: 2px 0.5%;
	border: solid #D7D6D6;
	border-width: 1px 0 0 0;
}
.selectedCommentAuthor
{
	position: relative;
	width: <?php echo (isIE() ? "100" : "99"); /* IE needs 100% not 99% */?>%;
	min-height: 17px;
	padding: 2px 0.5%;
	border: solid #D7D6D6;
	background: #FFF2E5;
	border-width: 1px 0 0 0;
}
.authorDetails
{
	background: #FCFB98;
}
.authorDetailsPreview, .reportDetailsPreview
{
	background: #FC8585;
}
.reportTag {
    text-align: right;
    padding-right: 10px;
    font-size: small;
}
.commentBlock
{
	border: 0;
	width: 100%;
	min-height: 17px;
	padding: 0;
	background: #FFCC99;
}
.selectedComment
{
	background: #FFF2E5;
}
.selectedComment pre {
	padding-top: 0;
	padding-bottom: 0;
	margin-top: 0;
	margin-bottom: 0;
}
.commentHdr
{
	background: #EFEFEF;
	font-weight: bold;
}
.sectionTask
{
 	position: relative;
 	float: left;
	width: 75%;
	padding: 2px 0 0 0;
}
.reportOptions
{
 	position: relative;
 	float: left;
	width: 25%;
	font-weight: normal;
}
.printIcon
{
	position: relative;
	float: right;
	padding: 3px 0 0 3px;
}

/************************
** Rounded DIV styles **
**********************/
.bl,.blHdr {background:url(../images/bl.gif) 0 100% no-repeat; width:100%;}
.br,.brHdr {background:url(../images/br.gif) 100% 100% no-repeat}
.blHdr {background: 0 100% no-repeat;}
.brHdr {background: 100% 100% no-repeat}
.tl,.tlCon {background:url(../images/tl.gif) 0 0 no-repeat}
.tr {background:url(../images/tr.gif) 100% 0 no-repeat; padding:9px;}
.tlCon { background:0 0 no-repeat;}
.trCon {background:url(../images/miniBorderPixel.gif) 100% 0 no-repeat; padding: 4px 10px 13px 4px;}
.t,.tCon {background:#D7D6D6 url(../images/greyGradient.gif) 0 0 repeat-x; width:99%;}
.tCon {background:#FFF;}
.b {background:url(../images/miniBorderPixel.gif) 0 100% repeat-x}
.bCon {background:url(../images/miniBorderPixelShadowedBelow.gif) 0 100% repeat-x}
.l {background:url(../images/miniBorderPixel.gif) 0 0 repeat-y}
.rHdr {background:url(../images/miniBorderPixelHdrShadowedRight.gif) 100% 0 repeat-y; min-height: 34px}
.r {background:url(../images/miniBorderPixelShadowedRight.gif) 100% 0 repeat-y}

.tClear {background: #FFF url(../images/clearGradient.gif) 0 0 repeat-x; width:99%;}
.tlClear {background: url(../images/tlClear.gif) 0 0 no-repeat; }
.tConClear { background:#FFF url(../images/miniBorderPixel.gif) 0 0 repeat-x; width:99%; margin: 10px 0 0 0; }

/*************************************
** DIVs used in outputting search. **
***********************************/
.coverDiv
{
	position: relative;
	float: left;
	left: -2px;
	width: 99%;
	padding: 0.5%;
	border: solid #CCC;
	border-width:  0 0 1px 0;
}
.coverHdrDiv
{
	position: relative;
	float: left;
	width: 29%;
	padding: 0 0.5%;
	font-weight: bold;
}
.coverDetailsDiv
{
	position: relative;
	float: left;
	width: 69%;
	padding: 0 0.5%;
}
.searchBtnDiv
{
	position: relative;
	width: 100%;
	left: -2px;
	min-height: 20px;
	margin: 0 0 10px 0;
}


/* === Attached images header === */
.attachedImagesHdr
{
	position: relative;
	width: 100%;
	min-height: 18px;
	height: 18px;
	padding: 2px;
	border: solid #D7D6D6;
	border-width: 0 1px;
}
/* === Attached images header (Comment) === */
.attachedImagesHdrComment
{
	position: relative;
	left: -2px;
	width: 100%;
	min-height: 18px;
	height: 18px;
	padding: 1px;
	background: #FFCC99;
	border: solid #D7D6D6;
	border-width: 0 1px;
}

/* === Attached images === */
.attachedImages, .attachedImagesPreview
{
	position: relative;
	width: 100%;
	padding: 2px;
	border: solid #D7D6D6;
	border-width: 0 1px;
}
/* === Attached images (Preview) === */
.attachedImagesPreview
{
	background: #FC8585;
}
/* === Attached images (Comment) === */
.attachedImagesComment
{
	position: relative;
	width: 100%;
	left: -2px;
	padding: 1px;
	background: #FFCC99;
	border: solid #D7D6D6;
	border-width: 0 1px;
}

/* === Clear block === */
.clearBlock
{
	position: relative;
	width: 100%;
	padding: 3px;
	height: 5px;
	border-top: solid 1px #D7D6D6;
}

/* === Logo title === */
.logoTitle
{
	float: left;
	line-height: 22px;
	font-size: 20px;
	padding: 8px 0 0 0;
	color: #198B3A;
}

/* === Central content === */
#centralContent
{
	position: relative;
	width: 98%;
	min-height: 100px;
	padding: 0 1% 10px 1%;
	background: #FFFFFF;
}

/* === Uploaded image === */
.uploadedImg
{
	position: relative;
	width: 100%;
	height: 20px;
}

/* === Uploaded fletype image === */
.uploadedFileType
{
	float: left;
	text-align: center;
	width: 3%;
	height: 20px;
}

/* === Uploaded filename === */
.uploadedFilename
{
	float: left;
	text-align: left;
	width: 90%;
	height: 20px;
}

/* === Warning message === */
.msg
{
	color: #FF0000;
	margin: 0 0 0 10px;
}

/* === Formatting break === */
.break
{
	clear: both;
}

/* === File type cover === */
.fileTypeCover
{
	position: relative;
	float: left;
	display: block;
	width: 100%;
	margin: 0 0 10px 0;
}

/* === File type === */
.fileType
{
	position: relative;
	float: left;
	display: block;
	height: 16px;
	width: 50px;
	margin: 0 0 4px 0;
}


/*********************
** Form containers **
*******************/
.formContainer
{
 	position: relative;
	float: left;
	width: 98%;
	margin: 5px 10px 0 1%;
}
.formHdr
{
	float: left;
	width: 15%;
	margin: 0 1% 0 0;
}
.formVal
{
	float: left;
	width: 84%;
}

/********************
** Editor options **
******************/
.sectionHdrWithEd
{
 	position: relative;
	float: left;
	width: 50%;
	min-width: 200px;
	margin: 0 10px 0 0;
}
.editorOptions
{
 	position: relative;
	float: right;
	top: -3px;
	margin: 0 10px 0 2px;
}

/********************
** Editor options **
******************/
.sectionHdrWithEd
{
 	position: relative;
	float: left;
	width: 50%;
	min-width: 200px;
	margin: 0 10px 0 0;
}
.editorOptions
{
 	position: relative;
	float: right;
	top: -3px;
	margin: 0 10px 0 2px;
}

